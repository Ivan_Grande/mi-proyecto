<%-- 
    Document   : Table
    Created on : May 24, 2019, 12:15:52 PM
    Author     : Ivan
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ include file="Modal/Modal_Detalle_Orden.jsp"%> 
<%@ include file="Conexion/Conexion.jsp"%> 
<!DOCTYPE html>
<%@ include file="Masterpages/header.jsp"%> 


<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2>
                    EXPORTABLE TABLE
                </h2>
                <ul class="header-dropdown m-r--5">
                    <li class="dropdown">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                            <i class="material-icons">more_vert</i>
                        </a>
                        <ul class="dropdown-menu pull-right">
                            <li><a href="javascript:void(0);">Action</a></li>
                            <li><a href="javascript:void(0);">Another action</a></li>
                            <li><a href="javascript:void(0);">Something else here</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
                    <div class="body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover dataTable  js-exportable">
                                <thead>
                                    <tr>
                                        <th>Compania</th>
                                        <th>Empleado</th>
                                        <th>Fecha</th>
                                        <th>Direccion</th>
                                        <th>Acciones</th>
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                        <th>Compania</th>
                                        <th>Empleado</th>
                                        <th>Fecha</th>
                                        <th>Direccion</th>
                                         <th>Acciones</th>
                                    </tr>
                                </tfoot>
                                <tbody>

                                    <%  st = conexion.prepareStatement("select Orders.OrderID,Customers.CompanyName,"
                                            + "CONCAT(Employees.FirstName,' ',"
                                            + "Employees.LastName)as Nombre_Empleado,"
                                            + "Orders.OrderDate,Orders.ShipAddress "
                                            + " from Orders inner join Customers on "
                                            + "(Customers.CustomerID = Orders.CustomerID)"
                                            + "inner join Employees on (Employees.EmployeeID = Orders.EmployeeID);"); 
                                        rs = st.executeQuery(); 
                                        while (rs.next()) {  %> 
                                        <tr>  
                                            <td><%=rs.getString("CompanyName")%> </td> 
                                            <td><%=rs.getString("Nombre_Empleado")%> </td> 
                                            <td><%=rs.getString("OrderDate")%>   </td> 
                                            <td><%=rs.getString("ShipAddress")%></td> 
                                            <th><button id="" 
                                                        class="btn btn-primary waves-effect" 
                                                        href="Modal_Detalle_Orden.jsp?Cod=<%=rs.getString("OrderID")%>" 
                                                        data-toggle="modal" data-target="#Modal_Detalle_Orden"> <i class="material-icons">search</i>
                                    <span>SEARCH</span> </button>
                                            </th>
                                        </tr> 
                                    <% }  conexion.close();  %> 

                                </tbody>
                            </table>
                        </div>
                    </div>
              </div>
        </div>
</div>

<%@ include file="Masterpages/footer.jsp"%> 

